define(function(require, exports, module) {
	main.consumes = ['Plugin', 'c9', 'proc', 'panels', 'tree'];
	main.provides = ["smf.utils"];
	return main;

	function main(options, imports, register) {
		var Plugin = imports.Plugin;
		var c9 = imports.c9;
		var proc = imports.proc;
		var panels = imports.panels;
		var tree = imports.tree;

		var plugin = new Plugin("Smartface.io", main.consumes);
		plugin.on("load", function() {});
		plugin.on("unload", function() {});

		plugin.freezePublicAPI({
			cli: cliHelpers(),
			datagrid: dataGridHelpers(),
			html: htmlHelpers(),
			vfs: vfsHelpers()
		});

		register(null, {
			"smf.utils": plugin
		});

		function vfsHelpers() {
			function revealFilePathInTree(path, noFocus) {
				console.log('revealing', path);
				panels.activate("tree");
				tree.expand(path, function(err) {
					if (!err) {
						tree.select(path);
					}
					tree.scrollToSelection();
				});
				if (!noFocus) {
					tree.focus();
				}
			}
			return {
				revealFilePathInTree: revealFilePathInTree
			}
		}

		function cliHelpers() {
			return {
				convertJsonToCliArguments: convertJsonToCliArguments,
				execSmfCommand: execSmfCommand
			};

			function convertJsonToCliArguments(obj) {
				var keys = Object.keys(obj);
				var args = [];
				for (var i = 0; i < keys.length; i++) {
					var key = keys[i];
					args.push('--' + key);
					args.push(obj[key]);
				}
				return args;
			}

			function execSmfCommand(command, callback) {
				proc.spawn("smartface-c9", {
					args: convertJsonToCliArguments({
						'command': command,
						'workspacePath': c9.workspaceDir
					})
				}, function(err, process) {
					if (err) throw err;
					process.stdout.on("data", function(chunk) {
						var response, err;
						try {
							response = JSON.parse(chunk);
							err = null;
						} catch (e) {
							err = 'There was a json parsing error';
							response = chunk;
						}
						callback(err, response);
					});
				});
			}
		}

		function dataGridHelpers() {
			function loop(item, callback) {
				if (!item) {
					return;
				}
				callback(item);
				if (item.children) {
					for (var i = 0; i < item.children.length; i++) {
						loop(item.children[i], callback);
					}
				}
			}

			function loop_reverse(item, callback) {
				if (!item) {
					return;
				}
				callback(item);
				if (item.parent) {
					loop_reverse(item.parent, callback);
				}
			}

			function collectCheckedItems(datagridInstance, shouldAllowItem) {
				var datagridChecked = [];
				shouldAllowItem = shouldAllowItem || function() {
					return true;
				};
				for (var i = 0; i < datagridInstance.root.children.length; i++) {
					var child = datagridInstance.root.children[i];
					loop(child, function(item) {
						if (shouldAllowItem(item)) {
							datagridChecked.push(item);
						}
					});
				}
				return datagridChecked;
			}
			return {
				loop: loop,
				loop_reverse: loop_reverse,
				collectCheckedItems: collectCheckedItems
			}
		}

		function getFormWrapperId(num) {
			return 'smf-publish-wizard-step' + num + '-form-wrapper';
		}

		function htmlHelpers() {
			function fragmentFromString(strHTML) {
				return document.createRange().createContextualFragment(strHTML);
			}

			function waitForElementById(id, callback, duration) {
				duration = duration || 100;
				var tid = setInterval(function() {
					var elem = document.getElementById(id);
					if (elem) {
						clearInterval(tid);
						callback();
					}
				}, duration);
			}

			return {
				fragmentFromString: fragmentFromString,
				waitForElementById: waitForElementById
			}
		}
	}
});