var SmartfaceDownloadFile;
define(function(require, exports, module) {
	main.consumes = ["Wizard", "WizardPage", "ui", "commands", "menus", "Form", 'Datagrid',
		'proc', 'c9', 'vfs', 'tabbehavior', 'layout', 'tabManager',
		"dialog.notification", "dialog.error",
		'smf.utils'
	];
	main.provides = ["smf.publish.wizard"];
	return main;

	function main(options, imports, register) {
		var Wizard = imports.Wizard;
		var WizardPage = imports.WizardPage;
		var ui = imports.ui;
		var layout = imports.layout;
		var menus = imports.menus;
		var commands = imports.commands;
		var Form = imports.Form;
		var c9 = imports.c9;
		var staticPrefix = options.staticPrefix || 'static/plugin';
		var vfs = imports.vfs;
		var tabManager = imports.tabManager;
		//var tabbehavior = imports.tabbehavior;

		var proc = imports.proc;
		var Datagrid = imports.Datagrid;
		var smfUtils = imports['smf.utils'];

		//var dialogNotify = imports["dialog.notification"].show;
		var dialogError = imports["dialog.error"].show;
		//add dialogSuccess

		var DATA = {
			appName: null,
			fetched: {},
			forms: {}
		};
		var PUBLISH_IN_PROGRESS = false;

		SmartfaceDownloadFile = function(file) {
			vfs.download(file, null, true);
		};

		var cancelButton, previousButton, nextButton, finishButton, buttonContainer;

		function buttonText(button, text) {
			var caption = button.children[0];
			if (typeof text === 'undefined') {
				return caption.textContent;
			} else {
				caption.textContent = text;
			}
		}

		//var showCloseButton = true;
		//dialogNotify("<div class='smf-dialog-notification'>Fetching Licenses!</div>", showCloseButton);

		var STEP1_FORM_DIV_WRAPPER_ID = getFormWrapperId('1');
		var STEP2_FORM_DIV_WRAPPER_ID = getFormWrapperId('2');
		//var licenses = {
		//	last_checked: 0,
		//	items: []
		//};
		var datagrid, spinner;
		//var datagridChecked = [];
		/***** Initialization *****/
		var FORMS = {};
		fetchAppName();

		var plugin = new Wizard("Smartface.io", main.consumes, {
			title: "Publish Wizard",
			"class": "smf-publish-wizard",
			allowClose: true,
			height: 500,
			width: 700,
			resizable: false
		});

		window.wizard = plugin;

		ui.insertCss(require("text!./publish-wizard.less"), staticPrefix, plugin);

		var step1 = createStep1(plugin);
		var step2 = createStep2(plugin);

		plugin.on("draw", function() {
			buttonContainer = document.querySelector(".smf-publish-wizard .dialog-buttons");
			cancelButton = buttonContainer.children[0];
			previousButton = buttonContainer.children[2];
			nextButton = buttonContainer.children[3];
			finishButton = buttonContainer.children[4];

			plugin.on("next", function(e) {
				var page = e.activePage;
				if (page.name === "step1") {
					nextButton && buttonText(nextButton, "Publish");
					if (datagrid) {
						fetchPackageProfiles(datagrid);
					}
					return step2;
				} else if (page.name === 'step2') {
					PUBLISH_IN_PROGRESS = true;
					var packages = DATA.forms.packagesToPublish = step2.collectSelected();
					exec_publish(packages);
					return step2;
				}
				plugin.allowClose = plugin.showNext = true;
				nextButton && buttonText(nextButton, "Next");
				plugin.showFinish = false;
				return step1;
			}, plugin);
			plugin.allowClose = plugin.showNext = true;
			plugin.startPage = step1;
		});

		plugin.on("load", function() {
			load();
		});

		plugin.on("unload", function() {});

		plugin.on("finish", function() {
			PUBLISH_IN_PROGRESS = false;
			plugin.allowClose = plugin.showNext = true;
			nextButton && buttonText(nextButton, "Next");
			plugin.showFinish = false;
			fetchPackageProfiles(datagrid);
			plugin.gotoPage(step1);
		});

		plugin.on("previous", function() {
			nextButton && buttonText(nextButton, "Next");
		});

		plugin.freezePublicAPI({});

		register(null, {
			"smf.publish.wizard": plugin
		});

		function afterPublishProcessFinish() {
			PUBLISH_IN_PROGRESS = false;
			buttonContainer.removeChild(spinner);
			plugin.showFinish = true;
		}

		function parseNodeJsonStringMessages(chunk) {
			var response;
			try {
				response = JSON.parse(chunk);
			} catch (e) {
				if (chunk.indexOf('}{') !== -1) {
					try {
						response = JSON.parse('[' + chunk.replace(/\}\{/g, '},{') + ']');
					} catch (e) {
						console.log(responses);
						console.log(chunk);
					}
				} else {
					response = {
						"err": "JSON Parse Error",
						"chunk": chunk
					};
				}
			}
			return response;
		}

		function handleNodeProcessStdOut(response, datagridItem, callback) {
			if (datagridItem.nodeProgressDone) {
				return;
			}
			if (response.err) {
				var msg;
				console.log(response);
				if (response.err === "JSON Parse Error") {
					console.log('There was a json parsing error', response.chunk);
				} else {
					msg = (response.msg ? response.msg : response.err);
					datagridItem.nodeProgressMsg = '<span style="color:red;">' + response.err + '</span>';
					afterPublishProcessFinish();
					FORMS.step2.refresh();
				}
				var msg_str = response.msg || '';
				var msg_detail = response.detail || '';
				tabManager.openFile("output/publish-error.log", true, function(err, tab) {
					var doc = tab.document;
					doc.value = response.err + '\n' + msg_str + '\n' + msg_detail;
					//console.log("The value is: ", doc.value);
				});
				return dialogError(msg);
			} else if (response.downloadFilePath) {
				datagridItem.downloadFilePath = response.downloadFilePath;
			} else if (response.responseDone) {
				var c9path = datagridItem.downloadFilePath.replace(c9.workspaceDir, '');
				datagridItem.nodeProgressDone = true;
				datagridItem.nodeProgressMsg = 'Please <span style="pointer-events: auto;cursor: pointer;color: #10b3c0;" onclick="SmartfaceDownloadFile(\'' + c9path + '\');">download your file</span>.';
				FORMS.step2.refresh();
				smfUtils.vfs.revealFilePathInTree(c9path);
				callback(null);
			} else if (response.msg) {
				datagridItem.nodeProgressMsg = response.msg;
				FORMS.step2.refresh();
				console.log(response.msg);
			} else {
				console.log(response);
			}
		}

		/***** Functions *****/
		function publishPackage(datagridItem, licenseName, callback) {
			FORMS.datagridItem = datagridItem;
			var pkg = packagePropertiesFromGridItem(datagridItem, licenseName);
			var argsObj = {
				"projectRoot": c9.workspaceDir,
				"config_file": "config/project.json",
				"platform": "c9",
				"task": pkg.task,
				"logStdOut": "json",
				"licenseFile": 'test-files/input/data2.sfd'
				//"logLevel": "debug"
			};
			if (pkg.task === 'android-full-publish') {
				argsObj.maxJavaMemory = "200m";
				// argsObj.licenseName = pkg.licenseName;
				argsObj.profile = pkg.build_type + ':' + pkg.packageProfiles;
			}
			var cli_args = smfUtils.cli.convertJsonToCliArguments(argsObj);

			proc.spawn("smartface", {
				args: cli_args,
				cwd: c9.workspaceDir,
			}, function(err, process) {
				if (err) throw err;
				process.stdout.on("data", function(chunk) {
					var response = parseNodeJsonStringMessages(chunk);
					if (!response) {
						return;
					}
					if (response instanceof Array) {
						response = response.pop();
					}
					handleNodeProcessStdOut(response, datagridItem, callback);
				});
				process.stderr.on("data", function(chunk) {
					console.log(chunk);
					dialogError(chunk);
				});
			});
		}

		function managePublish(packages, licenseName, callback) {
			//TODO: start publish here
			if (packages.length === 0) {
				console.log('No packages are requested to publish');
				return callback("No Packages");
			}
			spinner = document.getElementById('spinnerContainer').children[0].cloneNode(true);
			buttonContainer.appendChild(spinner);

			plugin.allowClose = plugin.showPrevious = plugin.showNext = false;
			publishNext(packages);

			function publishNext(packages) {
				if (packages.length === 0) {
					console.log('All packages are published');
					buttonContainer.removeChild(spinner);
					plugin.showFinish = true;
					return callback(null);
				}
				var pkg = packages.shift();
				publishPackage(pkg, licenseName, function(err, res) {
					if (err) {
						return;
					}
					publishNext(packages);
				});
			}
		}

		function exec_publish(datagridItems) {
			var form1 = FORMS.step1.toJson();
			var licenseName = form1['license-type'];
			managePublish(datagridItems, licenseName, function(msg) {
				if (!msg) {
					msg = 'all packages are done';
				}
				console.log(msg);
			});
		}

		function packagePropertiesFromGridItem(item, licenseName) {
			if (item.platform === 'ios') {
				return {
					"task": "ios-full-publish",
					"licenseName": licenseName,
					"packageProfiles": "Default"
				};
			} else if (item.platform === 'android') {
				return {
					"task": "android-full-publish",
					"licenseName": licenseName,
					"build_type": item.build_type,
					"packageProfiles": item.profile
				};
			}
			return false;
		}

		function getFormWrapperId(num) {
			return 'smf-publish-wizard-step' + num + '-form-wrapper';
		}

		function createStep1(plugin) {
			var step = new WizardPage({
				name: "step1",
				elements: [{
					type: "button",
					id: "next",
					caption: "Next",
					color: "aqua",
					"default": true,
					onclick: next
				}]
			}, plugin);

			step.on("draw", function(e) {
				e.html.style.paddingTop = "0px";
				ui.insertHtml(e.html, require("text!./step1.html"), step);
				afterHtmlStep1(e.html);
			});

			return step;

			function afterHtmlStep1(doc) {
				var div = doc.querySelector("#" + STEP1_FORM_DIV_WRAPPER_ID);
				var form = new Form({
					html: div,
					edge: "10 10 10 10",
					className: "smf-publish-wizard-step1-form",
					colwidth: 100,
					form: [{
						title: "License Name",
						type: "dropdown",
						name: "license-type",
						defaultValue: 'Smartface Demo',
						items: [{
							caption: "Smartface Demo",
							value: "Smartface Demo"
						}]
					}]
				});
				FORMS.step1 = form;
				//fetchLicenses(form);
			}

			/*function fetchLicenses(form) {
				setTimeout(function() {
					console.log('license-type updated');
					form.update([{
						id: "license-type",
						items: [{
							caption: "Community License",
							value: "Community License"
						}]
					}]);
					dialogError("Licenses Are Fake!");
				}, 10000);
			}*/
		}

		function createStep2(plugin) {
			var step = new WizardPage({
				name: "step2",
				elements: [{
					type: "button",
					id: "next",
					caption: "Generate",
					color: "aqua",
					"default": true,
					onclick: next
				}]
			}, plugin);

			step.on("draw", function(e) {
				e.html.style.paddingTop = "0px";
				ui.insertHtml(e.html, require("text!./step2.html"), step);
				afterHtmlStep2(e.html);
			});
			/* step.on('show', function() { afterHtmlStep2(step); }); */

			step.collectSelected = function() {
				return smfUtils.datagrid.collectCheckedItems(datagrid, shouldAllowItem);

				function shouldAllowItem(item) {
					if (!item.isChecked) {
						return false;
					}
					if (item.platform === 'ios') {
						return true;
					}
					if (item.profile) {
						return true;
					}
					return false;
				}
			};

			return step;

			function afterHtmlStep2(doc) {
				var div = doc.querySelector("#" + STEP2_FORM_DIV_WRAPPER_ID);
				datagrid = new Datagrid({
					container: div,
					enableCheckboxes: true,
					columns: [{
						caption: "Name",
						value: "label",
						width: "35%",
						type: "tree"
					}, {
						caption: "Description",
						dontEscapeHTML: true,
						getText: function(node) {
							return this.getValue(node);
						},
						getHTML: function(node) {
							return this.getValue(node);
						},
						getValue: function(node) {
							if (node.nodeProgressMsg) {
								return node.nodeProgressMsg;
							} else if (node.platform === 'ios') {
								return 'Will generate ' + DATA.appName + '.zip';
							} else if (node.platform === 'android') {
								if (node.profile) {
									return handleApkName(DATA.appName, node.profile, node.build_type);
								} else if (node.build_type) {
									return '';
								} else {
									return 'You can add more Package Profiles from PackageProfiles.xml';
								}
							}
						},
						width: "65%"
					}]
				}, step);

				function checkAll(target_item) {
					smfUtils.datagrid.loop(target_item, function(item) {
						datagrid.check(item);
					});
					var parent = target_item.parent;
					if (!parent) {
						return;
					}
					smfUtils.datagrid.loop_reverse(parent, function(item) {
						if (item.children) {
							for (var i = 0; i < item.children.length; i++) {
								var child = item.children[i];
								if (!child.isChecked) {
									datagrid.uncheck(item);
									return;
								}
							}
							datagrid.check(item);
						}
					});
				}

				datagrid.on("check", function(items) {
					var target_item = items[0];
					if (!target_item) {
						return;
					}
					if (PUBLISH_IN_PROGRESS) {
						datagrid.uncheck(target_item);
						return;
					}
					checkAll(target_item);
				});

				datagrid.on("uncheck", function(items) {
					var target_item = items[0];
					if (!target_item) {
						return;
					}
					if (PUBLISH_IN_PROGRESS) {
						datagrid.check(target_item);
						return;
					}
					smfUtils.datagrid.loop(target_item, function(item) {
						datagrid.uncheck(item);
					});
					smfUtils.datagrid.loop_reverse(target_item, function(item) {
						datagrid.uncheck(item);
					});
				});

				FORMS.step2 = datagrid;
				fetchPackageProfiles(datagrid);

				function handleApkName(appName, profile, build_type) {
					var str = appName;
					if (profile !== 'Default') {
						str += '-' + profile;
					}
					if (build_type !== 'ARM') {
						str += '-' + build_type;
					}
					return 'Will generate ' + str + '.apk';
				}

				function toggleAllChildren(widget, item, uncheck) {
					var checkMethod = (uncheck) ? 'uncheck' : 'check';
					if (item && item.children) {
						for (var i = 0; i < item.children.length; i++) {
							var child = item.children[i];
							widget[checkMethod](child);
							toggleAllChildren(widget, child, uncheck);
						}
					}
				}
			}
		}

		function fetchPackageProfiles(widget) {
			smfUtils.cli.execSmfCommand('getPackageProfiles', function(err, response) {
				if (err) {
					return console.error(err, response);
				}
				if (response.error) {
					return dialogError(response.error);
				}
				setProfileDataGridItems(widget, response.profiles);
			});
		}

		function fetchAppName(widget) {
			proc.spawn("smartface-c9", {
				args: smfUtils.cli.convertJsonToCliArguments({
					"command": 'getApplicationName',
					"workspacePath": c9.workspaceDir
				})
			}, function(err, process) {
				if (err) throw err;
				process.stdout.on("data", function(chunk) {
					try {
						var response = JSON.parse(chunk);
					} catch (e) {
						console.log('There was a json parsing error', chunk);
					}
					if (response) {
						if (response.error) {
							return dialogError(response.error);
						}
						DATA.appName = response.appName;
					}
				});
			});
		}

		function setProfileDataGridItems(datagrid, profiles) {
			var prof_items = [];
			var build_types = ['ARM', 'x86'];
			for (var i = 0; i < build_types.length; i++) {
				var build_type = build_types[i];
				prof_items.push(androidPlatformItem(build_type, profiles));
			}
			datagrid.setRoot({
				items: [{
					label: "iOS",
					platform: 'ios'
				}, {
					label: "Android",
					platform: 'android',
					isOpen: true,
					items: prof_items
				}]
			});
			datagrid.resize();

			function androidPlatformProfileItems(build_type, profiles) {
				var items = [];
				for (var i = 0; i < profiles.length; i++) {
					var profile = profiles[i];
					items.push({
						label: profile,
						platform: 'android',
						build_type: build_type,
						profile: profile
					});
				}
				return items;
			}

			function androidPlatformItem(build_type, profiles) {
				return {
					platform: 'android',
					build_type: build_type,
					label: build_type,
					isOpen: true,
					items: androidPlatformProfileItems(build_type, profiles)
				};
			}
		}

		function next() {
			console.log(plugin);
		}

		function addCommand() {
			commands.addCommand({
				name: "smf.PublishWizard",
				isAvailable: function() {
					return true;
				},
				exec: function() {
					plugin.show();
				}
			}, plugin);
			menus.addItemByPath("Tools/Publish", new ui.item({
				command: "smf.PublishWizard"
			}), 300, plugin);
		}

		function load() {
			ui.insertByIndex(layout.getElement('barTools'),
				new ui.button({
					id: "btnSmartfacePublish",
					skin: "c9-toolbarbutton-glossy",
					command: "smf.PublishWizard",
					caption: "Publish",
					disabled: false,
					class: "publishBtn",
					icon: "run.png",
				}), 1000, plugin);
			addCommand();
		}
	}
});